require("./env");
const express = require("express");
const app = express();

app.get("/", (req, res) => res.send("Hello world. I love my job").status(200));

const port = process.env.PORT;

app.listen(port, () => console.log(`Server is running on port: ${port}`));
