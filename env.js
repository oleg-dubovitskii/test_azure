const dotenv = require("dotenv");

dotenv.config({
  path: process.env.ENV_FILE || `${__dirname}/.env`,
});
